#!/bin/bash -
#===============================================================================
# Copyright (c) 2022 Petr Lautrbach
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -x

DATE=$(date +%Y%m%d%H%M%S)

BUILDROOT=`pwd`
BUILDDIR=$BUILDROOT/build
GITDIR=$BUILDROOT/git

REPO=${REPO:-https://github.com/SELinuxProject/selinux}
BRANCH=${BRANCH:-main}
REPODIR=${REPODIR:-$BUILDDIR/selinux}
PACKAGES=${PACKAGES:-libsepol libselinux libsemanage policycoreutils checkpolicy secilc}

COPRREPO=${COPRREPO:-plautrba/selinux-SELinuxProject}

mkdir -p $BUILDDIR

cd $BUILDDIR

if [ ! -d $REPODIR ]; then
    git clone $REPO $REPODIR
    cd $REPODIR
    git pull --tags
    cd -
else
    cd $REPODIR
    git fetch
    cd -
fi

for package in $PACKAGES; do

    export package
    if [ ! -d $package ]; then
        fedpkg clone -a $package
    else
        cd $package
        git fetch
        git reset --hard origin/rawhide
        cd -
    fi

    cd $package
    fedpkg sources

    gen_patch=$(sed -n '/git format-patch/{s/.*\(git format-patch.*\)/\1/;p}' $package.spec)
    cd $REPODIR
    git checkout origin/$BRANCH
    eval $gen_patch
    GITREF=$(git rev-parse --short HEAD)
    cd -

    rm *patch
    mv $REPODIR/*patch .

    bash <<'EOF' > $package.spec.new
    sed '/^# Patch list start/q' $package.spec
    i=1
    for j in 0*patch; do
        if [ "$j" != "0*patch" ]; then
            printf "Patch%04d: %s\n" $i $j
            i=$((i+1))
        fi
    done
    echo '# Patch list end'
    sed '0,/^# Patch list end/d' $package.spec
EOF

    # temporary fix until pathfix appears in %{_rpmconfigdir}/redhat in el9
    sed -i 's#\(%{__python3} %{_rpmconfigdir}/redhat/pathfix.py\) \(.*\)#%if 0%{?fedora}\n\1 \2\n%else\npathfix.py \2\n%endif#' $package.spec.new

    if false; then
    # temporary add python3-setuptools to BR
    sed -i -e '/BuildRequires:/ {
a BuildRequires: python3-setuptools
bx
:x ; N; bx }' $package.spec.new
    # temporary hack - see 4e562fa1ffbb
    sed -i 's/\(license .*\)COPYING/\1LICENSE/' $package.spec.new
    fi
    # temporary until getpolicyload is added to libselinux
    sed -i '\&%{_sbindir}/getpidprevcon&a %{_sbindir}/getpolicyload' $package.spec.new
    # temporary to check /ru/ man removal
    sed -i '\&/ru/man&d' $package.spec.new
    diff $package.spec $package.spec.new
    mv $package.spec.new $package.spec
    rpmdev-bumpspec -s $DATE.$GITREF $package.spec
    rm *.src.rpm
    fedpkg srpm
    echo copr build $COPRREPO $package-*.src.rpm
    copr build $COPRREPO $package-*.src.rpm || exit $?
    echo -e '\n\n\n********************\n\n\n'
    cd $BUILDDIR
done

