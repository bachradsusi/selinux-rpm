# SELinux rpms

Automatically build SELinux Userspace rpms from https://github.com/SELinuxProject/selinux.git or https://github.com/fedora-selinux/selinux.git in few easy steps locally and in Fedora COPR.

## plautrba/selinux-SELinuxProject

https://copr.fedorainfracloud.org/coprs/plautrba/selinux-SELinuxProject

Packages built from https://github.com/SELinuxProject/selinux.git - upstream sources without Fedora modifications and without python 3.

plautrba/selinux-SELinuxProject - ![latest SELinuxProject build status](https://copr.fedorainfracloud.org/coprs/plautrba/selinux-SELinuxProject/package/checkpolicy/status_image/last_build.png)

plautrba/selinux-fedora - ![latest Fedora build status](https://copr.fedorainfracloud.org/coprs/plautrba/selinux-fedora/package/checkpolicy/status_image/last_build.png)
